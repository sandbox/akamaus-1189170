<?php
/* SMS Pilot.Class API/PHP v1.8
 * SEE: http://www.smspilot.ru/apikey.php

 Example (send):
 	include('smspilot.class.php');
	$sms = new SMSPilot( 'XXXXXXXXXXXXYYYYYYYYYYYYZZZZZZZZXXXXXXXXXXXXYYYYYYYYYYYYZZZZZZZZ' );
	$sms->send( '79087964781', 'Привет, разработчик!');

 Example 2 (replace sender + send):
 	include('smspilot.class.php');
	$sms = new SMSPilot( 'XXXXXXXXXXXXYYYYYYYYYYYYZZZZZZZZXXXXXXXXXXXXYYYYYYYYYYYYZZZZZZZZ', 'WINDOWS-1251' );
 	if ($sms->send('79121234512','Завтра контрольная!', 'SCHOOL_N2')) {
		echo 'Сообщение успешно отправлено<br />';
		echo 'Цена='.$sms->cost.' кредитов<br />';
		echo 'Баланс='.$sms->balance.' кредитов<br />';
	} else
		echo 'Ошибка! '.$sms->error;

  Example 3 (send + get sms status):
 	include('smspilot.class.php');
	$sms = new SMSPilot( 'XXXXXXXXXXXXYYYYYYYYYYYYZZZZZZZZXXXXXXXXXXXXYYYYYYYYYYYYZZZZZZZZ' );
	$sms->send('79087964781', 'Novy zakaz http://www.smspilot.ru!');
	print_r( $sms->status[0] ); // Array ( [id] => 94 [phone] => 79087964781 [zone] => 2 [status] => 0 )
	print_r( $sms->statusByPhone( '79087964781' ) ); // // Array ( [id] => 94 [phone] => 79087964781 [zone] => 2 [status] => 0 )

  Example 5 (check status):
 	include('smspilot.class.php');
	$sms = new SMSPilot( 'XXXXXXXXXXXXYYYYYYYYYYYYZZZZZZZZXXXXXXXXXXXXYYYYYYYYYYYYZZZZZZZZ' );
	$status = $sms->check( array( 94,95) );
	print_r( $status );

	Array
(
    [0] => Array
        (
            [id] => 94
            [phone] => 79087964781
            [zone] => 2
            [status] => 2
        )

    [1] => Array
        (
            [id] => 95
            [phone] => 79087964781
            [zone] => 2
            [status] => -1
        )

)

  Example 6 ( balance  ):
 	include('smspilot.class.php');
	$sms = new SMSPilot( 'XXXXXXXXXXXXYYYYYYYYYYYYZZZZZZZZXXXXXXXXXXXXYYYYYYYYYYYYZZZZZZZZ' );
	echo $sms->balance(); // 23004

*/
class SMSPilot {
	public $apikey = 'XXXXXXXXXXXXYYYYYYYYYYYYZZZZZZZZXXXXXXXXXXXXYYYYYYYYYYYYZZZZZZZZ';
	public $charset = 'UTF-8';
	public $use_ssl = false;
	public $to;
	public $text;
	public $from = false;
	public $error;
	public $success;
	public $status; // new in 1.7
	public $info;
	public $cost; // new in 1.8
	public $balance; // new in 1.8
	// public $limit; // removed 1.8

	public $host = 'smspilot.ru';

	function __construct( $apikey = false,  $charset = false ) {
		if ($apikey)
			$this->apikey = $apikey;
		if ($charset)
			$this->charset = $charset;
	}
	// send sms via smspilot.ru
	function send( $to = false, $text = false, $from = false ) {

		if ($to)
			$this->to = $to;

		if ( $text )
			$this->text = $text;

		if ( $from )
			$this->from = $from;

		$text = ($this->charset != 'UTF-8') ? mb_convert_encoding($this->text, 'utf-8', $this->charset ) : $this->text;
		$text = urlencode($text);
		$to = ((is_array($to)) ? implode(',', $to) : $to);
		$to = urlencode($to);
		$from = urlencode($this->from);

		$this->error = false;
		$this->success = false;
		$this->status = array();

		$eol = "\r\n";

		$post = 'send='.$text
			.'&to='.$to
			.'&from='.$from
			.'&apikey='.$this->apikey;

		$header = 'POST /api.php HTTP/1.1'.$eol
			.'Host: '.$this->host.$eol
			.'Content-length: '.mb_strlen($post, 'latin1').$eol
			.'Content-type: application/x-www-form-urlencoded'.$eol
			.'Connection: close';

		$fp = @fsockopen( ($this->use_ssl ? 'ssl://' : '').$this->host, ($this->use_ssl) ? 443 : 80);
		if (!$fp) {
			$this->error = 'CONNECTION ERROR';
			return false;
		}
		$request = $header . $eol.$eol . $post . $eol.$eol;
//		echo $request;
		fwrite($fp, $request);
		$result = '';
		while ($r = fread($fp, 4096))
			$result .= $r;
		fclose($fp);

		$result = explode( $eol.$eol, $result );
		$result = $result[1];

//		echo $result;

		if ($result) {
			if (substr($result,0,6) == 'ERROR=') {
				$this->error = substr($result,6);
				return false;
			} elseif (substr($result,0,8) == 'SUCCESS=') {

				$this->success = substr($result,8,($p = strpos($result,"\n"))-8);

				if (preg_match('~(\d+)/(\d+)~', $this->success, $matches )) {
					$this->cost = $matches[1]; // new in 1.8
					$this->balance = $matches[2]; // new in 1.8
				}

				$status_csv = substr( $result, $p+1 );
				//status
				$status_csv = explode( "\n", $status_csv );
				foreach( $status_csv as $line ) {
					$s = explode(',', $line);
					$this->status[] = array(
						'id' => $s[0],
						'phone' => $s[1],
						'zone' => $s[2],
						'status' => $s[3]
					);
				}
				return $this->status;
			} else {
				$this->error = 'UNKNOWN RESPONSE';
				return false;
			}
		} else {
			$this->error = 'CONNECTION ERROR';
			return false;
		}
	}
	// check status by sms id or ids
	function check( $ids ) { // new in 1.7
		if (is_array($ids))
			$ids = implode(',', $ids);

		$this->error = false;
		$this->success = false;
		$this->status = array();

		$eol = "\r\n";

		$post = 'check='.$ids.'&apikey='.$this->apikey;

		$header = 'POST /api.php HTTP/1.1'.$eol
			.'Host: '.$this->host.$eol
			.'Content-length: '.mb_strlen($post, 'latin1').$eol
			.'Content-type: application/x-www-form-urlencoded'.$eol
			.'Connection: close'.$eol.$eol;

		$fp = @fsockopen( ($this->use_ssl ? 'ssl://' : '').$this->host, ($this->use_ssl) ? 443 : 80);
		if (!$fp) {
			$this->error = 'CONNECTION ERROR';
			return false;
		}
		$request = $header . $post . $eol . $eol;
//		echo $request;
		fwrite($fp, $request);
		$result = '';
		while ($r = fread($fp, 4096))
			$result .= $r;
		fclose($fp);

		$result = explode( $eol.$eol, $result );
		$result = $result[1];

		if ($result) {

			if (substr($result,0,6) == 'ERROR=') {

				$this->error = substr($result,6);
				return false;

			} else {

				$status_csv = $result;
				//status
				$status_csv = explode( "\n", $status_csv );
				foreach( $status_csv as $line ) {
					$s = explode(',', $line);
					$this->status[] = array(
						'id' => $s[0],
						'phone' => $s[1],
						'zone' => $s[2],
						'status' => $s[3]
					);
				}
				return $this->status;

			}
		} else {
			$this->error = 'CONNECTION ERROR';
			return false;
		}
	}
	// helper to find status by phone
	function statusByPhone( $phone ) {

		foreach( $this->status as $s )
			if ( $s['phone'] == $phone )
				return $s;

		return false;
	}
	function balance( $currency = 'sms' ) {

		$eol = "\r\n";

		$header = 'GET /api.php?balance='.$currency.'&apikey='.$this->apikey.' HTTP/1.1'.$eol
			.'Host: '.$this->host.$eol
			.'Connection: close'.$eol.$eol;

		$fp = @fsockopen( ($this->use_ssl ? 'ssl://' : '').$this->host, ($this->use_ssl) ? 443 : 80);
		if (!$fp) {
			$this->error = 'CONNECTION ERROR';
			return false;
		}
		$request = $header . $eol . $eol;

		fwrite($fp, $request );
		$result = '';
		while ($r = fread($fp, 4096))
			$result .= $r;
		fclose($fp);

		$result = explode( $eol.$eol, $result );
		$result = $result[1];

		if (strlen($result)) {
			if (substr($result,0,6) == 'ERROR=') {
				$this->error = substr($result, 6);
				return false;
			} else
				return $this->balance = $result;
		} else {
			$this->error = 'CONNECTION ERROR';
			return false;
		}

	}
	// apikey info
	function info( $apikey = NULL ) {

		if ($apikey === NULL)
			$apikey = $this->apikey;

		$eol = "\r\n";

		$header = 'GET /api.php?apikey='.$apikey.' HTTP/1.1'.$eol
			.'Host: '.$this->host.$eol
			.'Connection: close'.$eol.$eol;

		$fp = @fsockopen( ($this->use_ssl ? 'ssl://' : '').$this->host, ($this->use_ssl) ? 443 : 80);
		if (!$fp) {
			$this->error = 'CONNECTION ERROR';
			return false;
		}
		$request = $header . $eol . $eol;

		fwrite($fp, $request );
		$result = '';
		while ($r = fread($fp, 4096))
			$result .= $r;
		fclose($fp);

		$result = explode( $eol.$eol, $result );
		$result = $result[1];

		if ($result) {
			if (substr($result,0,6) == 'ERROR=') {
				$this->error = substr($result, 6);
				return false;

			} elseif (substr($result,0,8) == 'SUCCESS=') {
				$s = substr($result,8, ($p = strpos($result,"\n"))-8);

				$this->success = $s;

				$lines = explode("\n",substr($result,$p));

				$this->info = array();
				foreach( $lines as $line )
					if ($p = strpos($line,'='))
						$this->info[ substr($line,0,$p) ] = substr($line,$p+1);


				if ($this->charset != 'UTF-8')
					foreach( $this->info as $k => $v)
						$this->info[ $k ] = mb_convert_encoding($v,$this->charset,'UTF-8');

				return true;
			} else {
				$this->error = 'UNKNOWN RESPONSE';
				return false;
			}
		} else {
			$this->error = 'CONNECTION ERROR';
			return false;
		}
	}
}

?>