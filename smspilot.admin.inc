<?php

require 'smspilot.class.php';

function smspilot_admin_form($form, $form_state) {
  // Perform the query only if drawing the form
  if (!isset($form_state['input']['op']))
    $form['smspilot_balance'] = array(
      '#type' => 'item',
      '#title' => t('My SMS Pilot balance') . ': ' . smspilot_balance(),
    );

  $form['smspilot_sender'] = array(
    '#type' => 'textfield',
    '#title' => t('Sender'),
    '#description' => t('Your id shown to receiver'),
    '#size' => 10,
    '#maxlength' => 10,
    '#default_value' => isset($form_state['values']['smspilot_sender'])? $form_state['values']['smspilot_sender']:variable_get('smspilot_sender'),
  );

  $form['smspilot_default_recipient'] = array(
    '#type' => 'textfield',
    '#title' => t('Default recipient'),
    '#description' => t('Default number to send sms messages to'),
    '#size' => 11,
    '#maxlength' => 11,
    '#default_value' => isset($form_state['values']['smspilot_default_recipient'])? $form_state['values']['smspilot_default_recipient']:variable_get('smspilot_default_recipient'),
  );

  $form['smspilot_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#description' => t('The key you got from smspilot.ru'),
    '#size' => 80,
    '#maxlength' => 80,
    '#default_value' => isset($form_state['values']['smspilot_api_key'])? $form_state['values']['smspilot_api_key']:variable_get('smspilot_api_key'),
  );

  $form['smspilot_send_sms'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send SMS'),
    '#description' => t('Enable sending SMS to gateway. If unset, messages would only be logged'),
    '#default_value' => isset($form_state['values']['smspilot_send_sms'])? $form_state['values']['smspilot_send_sms']:variable_get('smspilot_send_sms',0),
  );

  return system_settings_form($form);
}

function smspilot_admin_form_validate($form, $form_state) {
  $sender = $form_state['values']['smspilot_sender'] = trim($form_state['values']['smspilot_sender']);
  $api_key = $form_state['values']['smspilot_api_key'] = trim($form_state['values']['smspilot_api_key']);

  $res = true;
  if (empty($sender)) {
    form_set_error('smspilot_sender',t('Input an alphanumeric identifier'));
    $res=false;
  }
  if (strlen($sender) >40) {
    form_set_error('smspilot_api_key',t("The key is probably invalid because it's too short"));
    $res=false;
  }
  return $res;
}

function smspilot_admin_form_submit($form, $form_state) {
  $sender = $form_state['values']['smspilot_sender'];
  $default_recipient = $form_state['values']['smspilot_default_recipient'];
  $api_key = $form_state['values']['smspilot_api_key'];
  $send_sms = $form_state['values']['smspilot_send_sms'];

  variable_set('smspilot_sender', $sender);
  variable_set('smspilot_default_recipient', $default_recipient);
  variable_set('smspilot_api_key', $api_key);
  variable_set('smspilot_send_sms', $send_sms);
}

function smspilot_balance($api_key=null) {
  if (empty($api_key))
    $api_key = variable_get('smspilot_api_key','');

  if (empty($api_key)) { // SMS Pilot class goes mad if we pass an empty key to it
    return '-';
  }

  $pilot = new SMSPilot($api_key);

  $result = $pilot->balance();
  if ($result === false) {
    $result = '-';
    drupal_set_message($pilot->error, 'error');
  }

  return $result;
}
